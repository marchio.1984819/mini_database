
// This represent a record in the only schema of this database
typedef struct Persona {
    char name[20];
    char surname[50];
    char address[100];
    int age;
} Persona;

// This is a node of an index that hold a string
typedef struct IndexNodeString {
    char * value;
    struct IndexNodeString * left;
    struct IndexNodeString * right;
} IndexNodeString;

// This is a node of an index that hold an int
typedef struct IndexNodeInt {
    int value;
    struct IndexNodeInt * left;
    struct IndexNodeInt * right;
} IndexNodeInt;

// A database hold a set of records and a set of indexes
typedef struct {
    IndexNodeString * name;
    IndexNodeString * surname;
    IndexNodeString * address;
    IndexNodeInt * age;
} Database;

// TODO implement the following methods
// The method return a Persona or NULL 

void insert(Database * database, Persona * persona);
Persona* findByName(Database * database, char * name);
Persona* findBySurname(Database * database, char * surname);
Persona* findByAddress(Database * database, char * address);
Persona* findByAge(Database * database, int age);
IndexNodeString* create_strings(char* string_name);
IndexNodeInt* create_int(int integer_name);
void insert_inorder_string(IndexNodeString* node, char* value);
void insert_inorder_int(IndexNodeInt* node, int value);
void freeIndexNodeString(IndexNodeString* node);
void freeIndexNodeInt(IndexNodeInt* node);
void free_database(Database* database);
void free_persona(Persona* people);
void print_tree_string(IndexNodeString * node, int level);
void print_tree_int(IndexNodeInt * node, int level);
void print_database(Database* database);
void print_persona(Persona* people);