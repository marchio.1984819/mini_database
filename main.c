#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

int main(){

    Persona person = {"Giuseppe", "Marchio", "Via Bho", 21};
    Persona person2 = {"Giuseppe2", "Marchio2", "Via Bho2", 22};
    Persona person3 = {"Giuseppe3", "Marchio3", "Via Bho3", 23};

    Database database;
    memset(&database, 0, sizeof(Database));
    insert(&database, &person);
    insert(&database, &person2);
    insert(&database, &person);
    insert(&database, &person3);

    printf("The database tree:\n");
    print_database(&database);

    // 

    printf("\nResearch of the name 'Giuseppe':\n");
    Persona* people = findByName(&database, "Giuseppe");
    print_persona(people);
    free_persona(people);

    printf("\nResearch of the surname 'Marchio3':\n");
    people = findBySurname(&database, "Marchio3");
    print_persona(people);
    free_persona(people);

    printf("\nResearch of the address 'Via Bho2':\n");
    people = findByAddress(&database, "Via Bho2");
    print_persona(people);
    free_persona(people);

    printf("\nResearch of the age '20':\n");
    people = findByAge(&database, 20);
    print_persona(people);
    free_persona(people);
    
    free_database(&database);

    return 0;
}