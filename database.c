#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

// function that free the memory occupied by the index node string
void freeIndexNodeString(IndexNodeString* node){
    if(node == NULL) return;

    free(node->value);
    freeIndexNodeString(node->left);
    freeIndexNodeString(node->right);
    free(node);
}

// function that free the memory occupied by the index node int
void freeIndexNodeInt(IndexNodeInt* node){
    if(node == NULL) return;

    freeIndexNodeInt(node->left);
    freeIndexNodeInt(node->right);
    free(node);
}

// function that free the memory occupied by the database
void free_database(Database* database){
    freeIndexNodeString(database->name);
    freeIndexNodeString(database->surname);
    freeIndexNodeString(database->address);
    freeIndexNodeInt(database->age);
}

// function that free the memory occupied by the Persona
void free_persona(Persona* people){
    if(people == NULL){
        return;
    }
    free(people);
}

// function that prints the tree of the index node string
void print_tree_string(IndexNodeString * node, int level) {
    if (node == NULL) {
        return;
    }
    for (int i = 0; i < level; i++)
        printf(i == level - 1 ? "| " : "  ");
    
    print_tree_string(node->left, level + 1);
    printf("%s\n", node->value);
    print_tree_string(node->right, level + 1);  
}

// function that prints the tree of the index node int
void print_tree_int(IndexNodeInt * node, int level) {
    if (node == NULL) {
        return;
    }
    for (int i = 0; i < level; i++)
        printf(i == level - 1 ? "| " : "  ");

    print_tree_int(node->left, level + 1);
    printf("%d\n", node->value);
    print_tree_int(node->right, level + 1);
}

// function that prints the database
void print_database(Database* database){
    print_tree_string(database->name, 0);
    printf("\n");
    print_tree_string(database->surname, 0);
    printf("\n");
    print_tree_string(database->address, 0);
    printf("\n");
    print_tree_int(database->age, 0);
}

// function that prints the Persona
void print_persona(Persona* people){
    if(people == NULL){
        printf("People not found!\n");
        return;
    }
    
    printf("%s\n", people->name);
    printf("%s\n", people->surname);
    printf("%s\n", people->address);
    printf("%d\n", people->age);

}

// function that create a pointer to a new memory area for index node string
IndexNodeString* create_strings(char* string_name){
    IndexNodeString* node = malloc(sizeof(IndexNodeString));
    if(node == NULL){
        return NULL;
    }
    // strdup is used to duplicate the string_name in node->value
    // it returns a pointer to a new memory area and with the copy of the string
    node->value = strdup(string_name);
    node->left = NULL;
    node->right = NULL;
    return node;
}

// function that insert inorder a value inside the node string
void insert_inorder_string(IndexNodeString* node, char* value){
    if(node == NULL){
        return;
    }

    if(strcmp(value, node->value) <= 0){
        if(node->left == NULL){
            IndexNodeString* root = create_strings(value);
            node->left = root;
            return;
        }
        insert_inorder_string(node->left, value);
        return;
    }
    if(node->right == NULL){
        IndexNodeString* root = create_strings(value);
        node->right = root;
        return;
    }
    insert_inorder_string(node->right, value);
    return;
}

// function that create a pointer to a new memory area for index node int
IndexNodeInt* create_int(int integer_name){
    IndexNodeInt* node = malloc(sizeof(IndexNodeInt));
    if(node == NULL){
        return 0;
    }
    node->value = integer_name;
    node->left = NULL;
    node->right = NULL;
    return node;
}

// function that insert inorder a value inside the node int
void insert_inorder_int(IndexNodeInt* node, int value){
    if(node == NULL){
        return;
    }

    if(value <= node->value){
        if(node->left == NULL){
            IndexNodeInt* root = create_int(value);
            node->left = root;
            return;
        }
        insert_inorder_int(node->left, value);
        return;
    }
    if(node->right == NULL){
        IndexNodeInt* root = create_int(value);
        node->right = root;
        return;
    }
    insert_inorder_int(node->right, value);
    return;
}


// function that puts inside the database all the data of the struct Persona
void insert(Database * database, Persona * persona){
    if(persona == NULL){
        return;
    }

    char* name = persona->name;
    char* surname = persona->surname;
    char* address = persona->address;
    int age = persona->age;

    // the first insert
    if(database->name == NULL){
        database->name = create_strings(persona->name);
        database->surname = create_strings(persona->surname);
        database->address = create_strings(persona->address);
        database->age = create_int(persona->age);
        return;
    }
    
    // the function insert_inorder_string is used for every char* data inside the struct Persona
    insert_inorder_string(database->name, name);

    insert_inorder_string(database->surname, surname);

    insert_inorder_string(database->address, address);

    // the function insert_inorder_int is used for every integer data inside the struct Persona
    insert_inorder_int(database->age, age);
    return;
}

// function that search a string value in a specific level
char* searchStringByLevel(IndexNodeString* node, int inizio, int level){
    if (node == NULL) {
        return NULL;
    }
    char* ret = node->value;
    if(inizio == level){
        return ret;
    }
    ret = searchStringByLevel(node->left, inizio + 1, level);
    ret = searchStringByLevel(node->right, inizio + 1, level);

    if(ret != NULL){
        return ret;
    }
}

// function that search an integer value in a specific level
int searchIntByLevel(IndexNodeInt* node, int inizio, int level){
    if (node == NULL) {
        return 0;
    }
    int eta = node->value;
    if(inizio == level){
       return eta;
    }
    eta = searchIntByLevel(node->left, inizio + 1, level);
    eta = searchIntByLevel(node->right, inizio + 1, level);

    if(eta != 0){
        return eta;
    }
}

// function that returns a pointer of Persona with name 'name'
Persona* findByName(Database * database, char * name){
    int level = 0;
    IndexNodeString* current = database->name;
    while(current != NULL){
        int cmp = strcmp(name, current->value);
        if(cmp == 0){
            // we are in the correct record
            // we have to implement a method that research the correct record
            // with name, surname, address and age
            char* surname = searchStringByLevel(database->surname, 0, level);
            char* address = searchStringByLevel(database->address, 0, level);
            int age = searchIntByLevel(database->age, 0, level);
            Persona* people = malloc(sizeof(Persona));
            strcpy(people->name, name);
            strcpy(people->surname, surname);
            strcpy(people->address, address);
            people->age = age;
            return people;
        }
        else if(cmp < 0){
            level++;
            current = current->left;
        }else{
            level++;
            current = current->right;
        }
    }
    return NULL; // no Persona with this name
}

// function that returns a pointer of Persona with surname 'surname'
Persona* findBySurname(Database * database, char * surname){
    // implements the same method of findByName
    // modify the elements name in surname

    int level = 0;
    IndexNodeString* current = database->surname;
    while(current != NULL){
        int cmp = strcmp(surname, current->value);
        if(cmp == 0){
            // we are in the correct record
            // we have to implement a method that research the correct record
            // with name, surname, address and age
            char* name = searchStringByLevel(database->name, 0, level);
            char* address = searchStringByLevel(database->address, 0, level);
            int age = searchIntByLevel(database->age, 0, level);
            Persona* people = malloc(sizeof(Persona));
            strcpy(people->name, name);
            strcpy(people->surname, surname);
            strcpy(people->address, address);
            people->age = age;
            return people;
        }
        else if(cmp < 0){
            level++;
            current = current->left;
        }else{
            level++;
            current = current->right;
        }
    }

    return NULL; // no Persona with this surname
}

// function that returns a pointer of Persona with address 'address'
Persona* findByAddress(Database * database, char * address){
    // implements the same method of findByName
    // modify the elements name in address
    int level = 0;
    IndexNodeString* current = database->address;
    while(current != NULL){
        int cmp = strcmp(address, current->value);
        if(cmp == 0){
            // we are in the correct record
            // we have to implement a method that research the correct record
            // with name, surname, address and age
            char* name = searchStringByLevel(database->name, 0, level);
            char* surname = searchStringByLevel(database->surname, 0, level);
            int age = searchIntByLevel(database->age, 0, level);
            Persona* people = malloc(sizeof(Persona));
            strcpy(people->name, name);
            strcpy(people->surname, surname);
            strcpy(people->address, address);
            people->age = age;
            return people;
        }
        else if(cmp < 0){
            level++;
            current = current->left;
        }else{
            level++;
            current = current->right;
        }
    }

    return NULL; // no Persona with this address

}

// function that returns a pointer of Persona with age 'age
Persona* findByAge(Database * database, int age){
    // implements the same method of findByName
    // modify the elements name in age
    int level = 0;
    IndexNodeInt* current = database->age;
    while(current != NULL){
        if(age == current->value){
            // we are in the correct record
            // we have to implement a method that research the correct record
            // with name, surname, address and age
            char* name = searchStringByLevel(database->name, 0, level);
            char* surname = searchStringByLevel(database->surname, 0, level);
            char* address = searchStringByLevel(database->address, 0, level);
            Persona* people = malloc(sizeof(Persona));
            strcpy(people->name, name);
            strcpy(people->surname, surname);
            strcpy(people->address, address);
            people->age = age;
            return people;
        }
        else if(age < current->value){
            level++;
            current = current->left;
        }else{
            level++;
            current = current->right;
        }
    }
    return NULL; // no Persona with this age
}